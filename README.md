# Vector's Avorion Shutdown Module

Add a shutdown command to your server to allow you to invoke
a timed shutdown procedure, with on-the-minute reminders.
