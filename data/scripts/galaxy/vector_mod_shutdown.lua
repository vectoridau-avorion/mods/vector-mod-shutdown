------
-- Shutdown Avorion Module Implementation.

package.path = package.path .. ";data/scripts/lib/?.lua"
include("stringutility")

------
-- Import the Shutdown Core Library.
local Shutdown = include("data/scripts/lib/vector_mod_shutdown")

-- Set tick values. Ticks are used to batch updates into time blocks
-- to improve server performance.
-- 6 ticks per minute == 10 seconds per tick

------
-- There are 6 ticks per minute.
local TICKS_PER_MINUTE = 6

------
-- There are 10 seconds between each tick.
local SECONDS_PER_TICK = 10

------
-- Module Get Update Interval Handler.
-- 
-- Configure the update interval for performance improvement.
function getUpdateInterval()
    return SECONDS_PER_TICK
end

------
-- Module Start Up Handler.
--
-- Initialize the shutdown library.
function initialize()
    print("Intializing vector_mod_shutdown"%_t)
    Shutdown.initialize(TICKS_PER_MINUTE)
end

------
-- Module Server Update Handler.
-- 
-- Process the next tick using the shutdown library.
function update(timeStep)
    if onServer() then
        Shutdown.processNextTick()
    end
end
