------
-- Shutdown Core Implementation.

package.path = package.path .. ";data/scripts/lib/?.lua"
include("stringutility")

------
-- Shutdown 'Class'
local Shutdown = {}

------
-- Module Configuration loaded from `data/config/vector_mod_shutdown.lua`
Shutdown.CONFIG = include("data/config/vector_mod_shutdown")

------
-- Constant: Server Value lookup key for the shutdown minute counter
Shutdown.KEY_MINUTES = "vector_shutdown__minutes"

------
-- Constant: Server Value lookup key for the shutdown tick counter
Shutdown.KEY_TICKS = "vector_shutdown__ticks"

------
-- Constant: Server Value lookup key for the shutdown tick batch size
Shutdown.KEY_TICK_BATCH_SIZE = "vector_shutdown__tick_batch_size"

------
-- Set the shutdown minute counter value.
function Shutdown.setMinutes(minute)
    Server():setValue(Shutdown.KEY_MINUTES, minute)
end

------
-- Get the shutdown minute counter value.
function Shutdown.getMinutes()
    return Server():getValue(Shutdown.KEY_MINUTES)
end

------
-- Set the shutdown tick counter value.
function Shutdown.setTicks(value)
    Server():setValue(Shutdown.KEY_TICKS, value)
end

------
-- Get the shutdown tick counter value.
function Shutdown.getTicks()
    return Server():getValue(Shutdown.KEY_TICKS)
end

------
-- Set the shutdown tick batch size value.
function Shutdown.setTickBatchSize(size)
    Server():setValue(Shutdown.KEY_TICK_BATCH_SIZE, size)
end

------
-- Get the shutdown tick batch size value.
function Shutdown.getTickBatchSize()
    return Server():getValue(Shutdown.KEY_TICK_BATCH_SIZE)
end

------
-- Reset the shutdown ticks counter.
function Shutdown.resetTicks()
    Shutdown.setTicks(Shutdown.getTickBatchSize())
end

------
-- Initialize the shutdown system.
-- @param tickBatchSize Set the number of ticks in a tick batch.
function Shutdown.initialize(tickBatchSize)
    Shutdown.clearVariables()
    Shutdown.setTickBatchSize(tickBatchSize)
end

------
-- Commence the shutdown process.
--
-- This function sets all appropriate variables, and
-- broadcasts a chat message to connected players informing
-- them that the shutdown is commencing.
-- 
-- @param minutes The total number of minutes to wait before
--   performing the shutdown.
-- @param message An additional informational message to display to
--   connected players.
function Shutdown.commence(minutes, message)
    Shutdown.setMinutes(minutes)
    Shutdown.resetTicks()

    -- Send the main shutdown notification
    Server():broadcastChatMessage(
        Shutdown.CONFIG.CHAT_SENDER_NAME,
        ChatMessageType.Information,
        Shutdown.CONFIG.SHUTDOWN_MESSAGE_INITIAL%_t % {minutes = minutes}
    )

    -- If the command includes a custom message then also
    -- send that message.
    if message ~= nil then
        Server():broadcastChatMessage(
            Shutdown.CONFIG.CHAT_SENDER_NAME,
            ChatMessageType.Information,
            message%_t
        )
    end
end

------
-- Cancel the shutdown process.
function Shutdown.cancel()
    local sender = Shutdown.CONFIG.CHAT_SENDER_NAME
    local message = Shutdown.CONFIG.SHUTDOWN_MESSAGE_CANCEL
    Server():broadcastChatMessage(
        sender,
        ChatMessageType.Information,
        message%_t
    )
    Shutdown.clearVariables()
end

------
-- Complete the shutdown process. Save the galaxy and stop the server.
function Shutdown.saveAndShutDownServer()
    Server():save()
    Server():stop()
end

------
-- Cancel or reset the shutdown process.
function Shutdown.clearVariables()
    Shutdown.setMinutes(nil)
    Shutdown.setTicks(nil)
end

------
-- Determine if a shutdown is in progress
function Shutdown.isInProgress()
    return Shutdown.getMinutes() ~= nil
end

------
-- Process the shutdown activity.
--
-- If a shutdown is in progress then perform the steps
-- necessary to advance that process.
function Shutdown.processNextTick()
    -- Skip if there is no shutdown in progress.
    if Shutdown.isInProgress() == false then
        return
    end

    -- Reduce the tick counter
    local ticks = Shutdown.getTicks()
    ticks = ticks - 1
    Shutdown.setTicks(ticks)

    -- If the tick counter falls below zero, then
    -- reset it to the tick batch size, and then process
    -- the next shutdown minute.
    if ticks <= 0 then
        Shutdown.resetTicks()
        Shutdown.processNextMinute()
    end
end

------
-- Process the next minute in the shutdown sequence.
--
-- Display any required messages for the given minute,
--  and invoke the shutdown command when required.
function Shutdown.processNextMinute()
    -- Reduce the minutes counter
    local minutes = Shutdown.getMinutes()
    minutes = minutes - 1
    Shutdown.setMinutes(minutes)

    -- If the minutes counter has an entry in the
    -- `SHUTDOWN_REMINDER_MESSAGES` table, then display
    -- that message.
    local reminder = Shutdown.CONFIG.SHUTDOWN_REMINDER_MESSAGES[minutes]
    if reminder ~= nil then
        local sender = Shutdown.CONFIG.CHAT_SENDER_NAME
        local type = reminder[1]
        local message = reminder[2]
        Server():broadcastChatMessage(sender, type, message)
    end

    -- If the minutes counter has reached or fallen below 0,
    -- then we should shut down the server now. Clear the
    -- variables for the shutdown module, and shut down the server.
    if minutes <= 0 then
        Shutdown.clearVariables()
        Shutdown.saveAndShutDownServer()
    end
end

return Shutdown
