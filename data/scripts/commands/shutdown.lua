------
-- Shutdown Command Implementation

package.path = package.path .. ";data/scripts/lib/?.lua"
include("stringutility")

------
-- Import the Shutdown Core Library.
local Shutdown = include("data/scripts/lib/vector_mod_shutdown")

------
-- Module Namespace.
--
-- The following namespace comment is *required* for
-- avorion to recognise the namespace. Do not alter or remove it.
--
-- namespace ShutdownCommand
ShutdownCommand = {}

------
-- Invoke the shutdown command.
-- @param sender The index of the player invoking the command.
-- @param commandName
-- @param minutes The number of minutes to set the shutdown timer for
-- @param ... An optional message to display to users.
function ShutdownCommand.execute(sender, commandName, minutes, ...)
    if (minutes == "cancel") then
        Shutdown.cancel()
        return 0, "", ""
    end

    -- If minutes is not set, then default to a 1 minute shutdown
    if minutes == nil then
        minutes = 1
    end

    -- Attempt to parse minutes as a number or leave if not valid
    minutes = tonumber(minutes)
    if minutes == nil or minutes <= 0 then
        return 0, "", ""
    end

    -- Gather all additional args and form them into a single string
    -- to push through as an extra message to display.
    local args = {...}
    local message = nil
    if #args > 0 then
        message = table.concat(args, " ")
    end

    -- Commence the shutdown process
    Shutdown.commence(minutes, message)

    -- Mandatory for Avorion commands
    return 0, "", ""
end

------
-- Provide a description of this command.
-- @return A string description of this command.
function ShutdownCommand.getDescription()
    return "Commence a timed server shutdown"%_t
end

------
-- Provide help for usage of this command.
-- @return A string describing the usage of this command.
function ShutdownCommand.getHelp()
    return "Commence a timed server shutdown. Usage: /shutdown <minutes> [message]"%_t
end
