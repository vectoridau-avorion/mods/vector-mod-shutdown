------
-- Configuration for Vector's Avorion Shutdown Module.

------
-- The configuration table for Vector's Avorion Shutdown Module.
local config = {}

------
-- The sender name to use when sending shutdown messages to players.
config.CHAT_SENDER_NAME = "SERVER SHUTDOWN"

------
-- The initial shutdown message to display when performing a timed shutdown.
config.SHUTDOWN_MESSAGE_INITIAL = "This server will SHUT DOWN in ${minutes} minute(s)."

------
-- The message to display if the shutdown is cancelled.
config.SHUTDOWN_MESSAGE_CANCEL = "The server shut down was cancelled."

------
-- A table of shutdown reminder messages to display at different
-- times during a shutdown.
-- 
-- The keys in the table represent the time (number of minutes remaining) at
-- which the message should be displayed. For example, the message at key [3] will
-- be displayed at 3 minutes before automatic shutdown.
-- 
-- Each entry in the table is an array containing the chat message type and the
-- actual message to display to connected users.
config.SHUTDOWN_REMINDER_MESSAGES = {
	[10] = {ChatMessageType.Information, "This server will shut down in 10 minutes."},
	[5] = {ChatMessageType.Information, "This server will shut down in 5 minutes."},
	[3] = {ChatMessageType.Warning, "This server will shut down in 3 minutes. Please complete your current task and log off."},
	[2] = {ChatMessageType.Warning, "This server will shut down in 2 minutes. Complete your task and log off now."},
	[1] = {ChatMessageType.Error, "60 SECONDS UNTIL SERVER SHUT DOWN. LOG OFF IMMEDIATELY TO SAVE YOUR PROGESS!"},
}

return config