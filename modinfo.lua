------
--- Module Information Package for Avorion's Module System.

------
-- The meta table is defined for use by Avorion's module system.
meta = {}

------
-- The globally unique ID for this module.
meta.id = "2058062688"

------
-- The type of mod ('mod' or 'factionpack')
meta.type = "mod"

------
-- A unique name for this module.
meta.name = "au.id.vector.avorion.mod.shutdown"

------
-- The human-readable title for this module.
meta.title = "Vector's Advanced Shutdown Mod"

------
-- A short description for this module.
meta.description = "This mod provides advanced server shutdown mechanisms."

------
-- An array of authors for this module.
meta.authors = {"Daniel 'Vector' Kerr"}

------
-- A valid version string for this module.
meta.version = "0.0.1"

------
-- Any modules that this module depends on.
meta.dependencies = {
    {id = "Avorion", min = "1.0", max = "1.0"}
}

------
-- A boolean indicating if this module only runs on the server side.
meta.serverSideOnly = true

------
-- A boolean indicating if this module only runs on the client side.
meta.clientSideOnly = false

------
-- A boolean indicating if this module makes changes that could
--   cause problems if the module is later uninstalled or disabled.
meta.saveGameaAltering = false

------
-- Contact details for questions relating to this module.
meta.contact = "avorion+shutdown-mod@vector.id.au"

return meta
